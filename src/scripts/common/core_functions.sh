export PRIMARY_REPO_NAME="Primary"
export REPO_STORAGE_PATH="${HOME}/repos"

export EXC_OK=0
export EXC_REPO_NOT_FOUND=15
export EXC_REPO_CLOSE_FAILED=16

if [[ $(id -u) -ne 0 ]]; then
  export SUDO="/usr/bin/env sudo"
else
  export SUDO=""
fi

fabric_string_to_lower() {
  echo "${1}" | tr '[:upper:]' '[:lower:]'
}

fabric_repo_resolve_name() {
  typeset REPO_NAME="$1"

  # If REPO_NAME was not set then default to primary
  if [[ -z "${REPO_NAME}" ]]; then
    typeset REPO_NAME="${PRIMARY_REPO_NAME}"
  fi

  fabric_string_to_lower "${REPO_NAME}"
}

fabric_repo_resolve_path() {
  echo "${REPO_STORAGE_PATH}/$(fabric_repo_resolve_name "$1")"
}

fabric_repo_open() {
  typeset REPO_NAME=$(fabric_repo_resolve_name "$1")
  typeset REPO_PATH=$(fabric_repo_resolve_path "$1")

  if [[ -d "${REPO_PATH}" ]]; then
    pushd "${REPO_PATH}" > /dev/null || return $?
  elif [[ "${REPO_NAME}" == "$(fabric_string_to_lower "${PRIMARY_REPO_NAME}")" ]]; then
    return ${EXC_REPO_NOT_FOUND}
  else
    mkdir -p "${REPO_PATH}" || return $?
    pushd "${REPO_PATH}" > /dev/null || return $?
  fi

  return ${EXC_OK}
}

fabric_repo_close() {
  typeset REPO_PATH=$(fabric_repo_resolve_path "$1")
  typeset CWD="$(pwd)"

  if [[ "${CWD#$REPO_PATH}" == "${CWD}" ]]; then
    return ${EXC_REPO_CLOSE_FAILED}
  fi

  popd > /dev/null || return $?

  return ${EXC_OK}
}

fabric_mvn_fetch_settings() {
  if [[ -f ".circleci_settings.xml" ]]; then
    echo "-s .circleci_settings.xml"
  elif [[ -f ".circleci/.circleci_settings.xml" ]]; then
    echo "-s .circleci/.circleci_settings.xml"
  else
    echo ""
  fi
}

export fabric_string_to_lower
export fabric_repo_resolve_name
export fabric_repo_resolve_path
export fabric_repo_open
export fabric_repo_close
export fabric_mvn_fetch_settings
export FABRIC_CORE_INSTALLED=1