description: >
  Performs a fail-fast maven compilation of the Git repository specified by the `alias` parameter. Defaults to
  compiling the primary repository if no value is specified.

  This command will look for the `.circleci.settings.xml` file in the repository root and if found will pass those
  settings to the maven commands.

  NOTE:
    The parameters javadoc_enabled and javadoc_aggregate_enabled are not mutually exclusive. If the javadoc_enabled
    parameter is set to false then no javadoc will be generated and the javadoc_aggregate_enabled parameter will have
    no effect.

  Command Requirements:
    - maven (>= 3.6.1)
    - openjdk (>= 12.0.2)

parameters:
  alias:
    type: string
    default: "Primary"
    description: >
      The local alias to use for this Git working copy. The supplied name will be converted to lowercase when building
      the local directory path. The command will place the Git working copy in the following location
      as specified by this template: `/root/repos/< lowercase(name) >`.
  clean_enabled:
    type: boolean
    default: true
    description: >
      Determines whether or not maven clean is run before compilation.
  compile_enabled:
    type: boolean
    default: true
    description: >
      Determines whether or not maven compile should be run.
  install_enabled:
    type: boolean
    default: false
    description: >
      Determines whether or not maven install is run after a successful compilation.
  deploy_enabled:
    type: boolean
    default: false
    description: >
      Determines whether or not maven deploy is run after a successful compilation.
  deploy_profile:
    type: string
    default: ""
    description: >
      An optional profile to be enabled when running maven deployment.
  javadoc_enabled:
    type: boolean
    default: false
    description: >
      Determines whether or not maven javadoc is run after a successful compilation.
  javadoc_aggregate_enabled:
    type: boolean
    default: false
    description: >
      Determines whether or not the maven javadoc command should use aggregate mode.
  javadoc_profile:
    type: string
    default: ""
    description: >
      An optional profile to be enabled when running maven javadoc.
  cache_enabled:
    type: boolean
    default: true
    description: >
      Controls whether dependency caching is enabled.
  cache_key:
    type: string
    default: v1-mvn-dependencies-{{ checksum "pom.xml" }}
    description: >
      The specific cache key template to use when looking up the dependency cache.
  cache_key_fallback:
    type: string
    default: v1-mvn-dependencies-
    description: >
      The fallback cache key template to use when looking up the dependency cache.
  when:
    type: string
    default: on_success
    description: >
      Specify when to enable or disable the step. Takes the following values: always, on_success, on_fail.

steps:
  - when:
      condition: << parameters.cache_enabled >>
      steps:
        - restore_cache:
            name: Restoring Maven Dependency Cache
            keys:
              - << parameters.cache_key >>
              - << parameters.cache_key_fallback >>
  - when:
      condition: << parameters.clean_enabled >>
      steps:
        - run:
            name: Maven Clean (<< parameters.alias >>)
            command: |
              fabric_repo_open "<< parameters.alias >>"
              MAVEN_SETTINGS="$(fabric_mvn_fetch_settings)"
              set -x
              mvn ${MAVEN_SETTINGS} -DskipTests clean
              set +x
              fabric_repo_close "<< parameters.alias >>"
            when: << parameters.when >>
  - when:
      condition: << parameters.compile_enabled >>
      steps:
        - run:
            name: Maven Compile (<< parameters.alias >>)
            command: |
              fabric_repo_open "<< parameters.alias >>"
              MAVEN_SETTINGS="$(fabric_mvn_fetch_settings)"
              set -x
              mvn ${MAVEN_SETTINGS} -DskipTests compile
              set +x
              fabric_repo_close "<< parameters.alias >>"
            when: << parameters.when >>
  - when:
      condition: << parameters.cache_enabled >>
      steps:
        - save_cache:
            name: Saving Maven Dependency Cache
            key: << parameters.cache_key >>
            paths:
              - ~/.m2/repository
  - when:
      condition: << parameters.javadoc_enabled >>
      steps:
        - unless:
            condition: << parameters.javadoc_aggregate_enabled >>
            steps:
              - run:
                  name: Maven Javadoc (<< parameters.alias >>)
                  command: |
                    JAVADOC_PROFILE="<< parameters.javadoc_profile >>"
                    JAVADOC_ARGS=""

                    if [[ -n "${JAVADOC_PROFILE}" ]]; then
                      JAVADOC_ARGS="${JAVADOC_ARGS} -P${JAVADOC_PROFILE}"
                    fi

                    fabric_repo_open "<< parameters.alias >>"
                    MAVEN_SETTINGS="$(fabric_mvn_fetch_settings)"
                    set -x
                    mvn ${MAVEN_SETTINGS} -DskipTests ${JAVADOC_ARGS} javadoc:javadoc
                    set +x
                    fabric_repo_close "<< parameters.alias >>"
                  when: << parameters.when >>
        - when:
            condition: << parameters.javadoc_aggregate_enabled >>
            steps:
              - run:
                  name: Maven Javadoc Aggregate (<< parameters.alias >>)
                  command: |
                    JAVADOC_PROFILE="<< parameters.javadoc_profile >>"
                    JAVADOC_ARGS=""

                    if [[ -n "${JAVADOC_PROFILE}" ]]; then
                      JAVADOC_ARGS="${JAVADOC_ARGS} -P${JAVADOC_PROFILE}"
                    fi

                    fabric_repo_open "<< parameters.alias >>"
                    MAVEN_SETTINGS="$(fabric_mvn_fetch_settings)"
                    set -x
                    mvn ${MAVEN_SETTINGS} -DskipTests javadoc:aggregate
                    set +x
                    fabric_repo_close "<< parameters.alias >>"
                  when: << parameters.when >>
  - when:
      condition: << parameters.install_enabled >>
      steps:
        - run:
            name: Maven Install (<< parameters.alias >>)
            command: |
              fabric_repo_open "<< parameters.alias >>"
              MAVEN_SETTINGS="$(fabric_mvn_fetch_settings)"
              set -x
              mvn ${MAVEN_SETTINGS} -DskipTests install
              set +x
              fabric_repo_close "<< parameters.alias >>"
            when: << parameters.when >>
  - when:
      condition: << parameters.deploy_enabled >>
      steps:
        - run:
            name: Maven Deploy (<< parameters.alias >>)
            command: |
              DEPLOY_PROFILE="<< parameters.deploy_profile >>"
              DEPLOY_ARGS=""

              if [[ -n "${DEPLOY_PROFILE}" ]]; then
                DEPLOY_ARGS="${DEPLOY_ARGS} -P${DEPLOY_PROFILE}"
              fi

              fabric_repo_open "<< parameters.alias >>"
              MAVEN_SETTINGS="$(fabric_mvn_fetch_settings)"
              set -x
              mvn ${MAVEN_SETTINGS} -DskipTests ${DEPLOY_ARGS} deploy
              set +x
              fabric_repo_close "<< parameters.alias >>"
            when: << parameters.when >>

